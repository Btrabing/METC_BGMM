import numpy as np
from sklearn.mixture import BayesianGaussianMixture
import pandas as pd
import matplotlib.pyplot as plt
import datetime as dt
import matplotlib as mpl
import matplotlib.gridspec as gridspec
import cartopy
import cartopy.crs as ccrs
import cartopy.mpl.ticker
from descartes import PolygonPatch
from matplotlib.collections import LineCollection
import glob
import matplotlib.patheffects as PathEffects
import matplotlib.text as mtext
import matplotlib.ticker as mticker
import matplotlib.transforms as mtrans
from matplotlib.colors import ListedColormap
import matplotlib.patches as mpatches
from cartopy.mpl import gridliner
from matplotlib.patches import Ellipse
from shapely.geometry import Polygon
from shapely.geometry import Point
from shapely.ops import unary_union
import cartopy.io.shapereader as shpreader
import cartopy.feature as cfeature
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
#import geopandas
import xarray as xr

import warnings
warnings.filterwarnings("ignore")

import subprocess
import sys

from Bias_Functions import *
#------------------------------------------------------
#
#
#
#
#
#
#
#

# Note that STORMNAMES and INITTIMES are zipped, so they must match shape
STORMNAMES = ['al092022']   # Specify the stormname as a list
INITTIMES = ['2022092612']   # Specify the time to run on as a list










#------------------------------------------------------

def get_gefs(file_path, stormtime=False):
    '''
    This code will grab all the gefs forecasts that are available in the adeck file
    The output will be the formatted data for the storm

    input atcf path with storm name attached to end
    input datetime as a list of values that should be included

    output numpy arrays of
    datetime YYYYMMDDHH
    model ensemble member identifier
    forecast hour as int
    lat in deg as float
    lon in deg as float
    intensity in kt as int



   '''

    dtg_ALL      = []
    model_ALL    = []
    fhr_ALL      = []
    lat_ALL      = []
    lon_ALL      = []
    int_ALL      = []


    #This will pull all the forecast data and remove any 50 or 64 kt wind duplicates
    data_list        = str(subprocess.check_output(("grep " + "AP "+file_path+" | awk -F', ' '{ if(substr($12,length($12)-1,2)!=\"50\" && substr($12,length($12)-1,2)!=\"64\") print}' | sort -u"),shell=True).decode('utf8')).splitlines()

    for data in data_list:
        line = data.split(',')
        #Add the data to the empty list
        dtg_ALL.append(line[2].strip(' '))
        model_ALL.append(line[4].strip(' '))
        fhr_ALL.append(int(line[5].strip(' ')))

        #Check which hemisphere the latitude is to format as a float
        if line[6].strip(' ')[-1]=='N':
            lat_ALL.append(float(line[6].strip(' ')[:-1])/10.)
        elif line[6].strip(' ')[-1]=='S':
            lat_ALL.append(-float(line[6].strip(' ')[:-1])/10.)
        else:
            lat_ALL.append(np.nan)

        #Now check which hemisphere the longitude is to format as float
        if line[7].strip(' ')[-1]=='E':
            lon_ALL.append(float(line[7].strip(' ')[:-1])/10.)
        elif line[7].strip(' ')[-1]=='W':
            lon_ALL.append(-float(line[7].strip(' ')[:-1])/10.)
        else:
            lon_ALL.append(np.nan)

        int_ALL.append(int(line[8].strip(' ')))
        


    dtg_ALL     = np.array(dtg_ALL)
    model_ALL   = np.array(model_ALL)
    fhr_ALL     = np.array(fhr_ALL)
    lat_ALL     = np.array(lat_ALL)
    lon_ALL     = np.array(lon_ALL)
    int_ALL     = np.array(int_ALL)


    if stormtime:
        return dtg_ALL[np.isin(dtg_ALL,stormtime)], model_ALL[np.isin(dtg_ALL,stormtime)], fhr_ALL[np.isin(dtg_ALL,stormtime)], lat_ALL[np.isin(dtg_ALL,stormtime)], lon_ALL[np.isin(dtg_ALL,stormtime)], int_ALL[np.isin(dtg_ALL,stormtime)]


    else:
        return dtg_ALL, model_ALL, fhr_ALL, lat_ALL, lon_ALL, int_ALL

def get_ukme(file_path, stormtime=False):
    '''
    This code will grab all the ukmetoff ensemble forecasts that are available in the adeck file
    The output will be the formatted data for the storm

    input atcf path with storm name attached to end
    input datetime as a list of values that should be included

    output numpy arrays of
    datetime YYYYMMDDHH
    model ensemble member identifier
    forecast hour as int
    lat in deg as float
    lon in deg as float
    intensity in kt as int



   '''

    dtg_ALL      = []
    model_ALL    = []
    fhr_ALL      = []
    lat_ALL      = []
    lon_ALL      = []
    int_ALL      = []

    for num in np.arange(0,36,1):
        ens_name = 'UE'+'%0.2d'%num
        #This will pull all the forecast data and remove any 50 or 64 kt wind duplicates
        data_list        = str(subprocess.check_output(("grep " + ens_name + " "+file_path+" | awk -F', ' '{ if(substr($12,length($12)-1,2)!=\"50\" && substr($12,length($12)-1,2)!=\"64\") print}' | sort -u"),shell=True).decode('utf8')).splitlines()

        for data in data_list:
            line = data.split(',')
            #Add the data to the empty list
            dtg_ALL.append(line[2].strip(' '))
            model_ALL.append(line[4].strip(' '))
            fhr_ALL.append(int(line[5].strip(' ')))

            #Check which hemisphere the latitude is to format as a float
            if line[6].strip(' ')[-1]=='N':
                lat_ALL.append(float(line[6].strip(' ')[:-1])/10.)
            elif line[6].strip(' ')[-1]=='S':
                lat_ALL.append(-float(line[6].strip(' ')[:-1])/10.)
            else:
                lat_ALL.append(np.nan)

            #Now check which hemisphere the longitude is to format as float
            if line[7].strip(' ')[-1]=='E':
                lon_ALL.append(float(line[7].strip(' ')[:-1])/10.)
            elif line[7].strip(' ')[-1]=='W':
                lon_ALL.append(-float(line[7].strip(' ')[:-1])/10.)
            else:
                lon_ALL.append(np.nan)

            int_ALL.append(int(line[8].strip(' ')))



    dtg_ALL     = np.array(dtg_ALL)
    model_ALL   = np.array(model_ALL)
    fhr_ALL     = np.array(fhr_ALL)
    lat_ALL     = np.array(lat_ALL)
    lon_ALL     = np.array(lon_ALL)
    int_ALL     = np.array(int_ALL)


    if stormtime:
        return dtg_ALL[np.isin(dtg_ALL,stormtime)], model_ALL[np.isin(dtg_ALL,stormtime)], fhr_ALL[np.isin(dtg_ALL,stormtime)], lat_ALL[np.isin(dtg_ALL,stormtime)], lon_ALL[np.isin(dtg_ALL,stormtime)], int_ALL[np.isin(dtg_ALL,stormtime)]


    else:
        return dtg_ALL, model_ALL, fhr_ALL, lat_ALL, lon_ALL, int_ALL

    
    
def get_eps(file_path, stormtime=False):
    '''
    This code will grab all the ukmetoff ensemble forecasts that are available in the adeck file
    The output will be the formatted data for the storm

    input atcf path with storm name attached to end
    input datetime as a list of values that should be included

    output numpy arrays of
    datetime YYYYMMDDHH
    model ensemble member identifier
    forecast hour as int
    lat in deg as float
    lon in deg as float
    intensity in kt as int



   '''

    dtg_ALL      = []
    model_ALL    = []
    fhr_ALL      = []
    lat_ALL      = []
    lon_ALL      = []
    int_ALL      = []

    for num in np.arange(0,25,1):
        ens_name = 'EN'+'%0.2d'%num
        #This will pull all the forecast data and remove any 50 or 64 kt wind duplicates
        data_list        = str(subprocess.check_output(("grep " + ens_name + " "+file_path+" | awk -F', ' '{ if(substr($12,length($12)-1,2)!=\"50\" && substr($12,length($12)-1,2)!=\"64\") print}' | sort -u"),shell=True).decode('utf8')).splitlines()

        for data in data_list:
            line = data.split(',')
            #Add the data to the empty list
            dtg_ALL.append(line[2].strip(' '))
            model_ALL.append(line[4].strip(' '))
            fhr_ALL.append(int(line[5].strip(' ')))

            #Check which hemisphere the latitude is to format as a float
            if line[6].strip(' ')[-1]=='N':
                lat_ALL.append(float(line[6].strip(' ')[:-1])/10.)
            elif line[6].strip(' ')[-1]=='S':
                lat_ALL.append(-float(line[6].strip(' ')[:-1])/10.)
            else:
                lat_ALL.append(np.nan)

            #Now check which hemisphere the longitude is to format as float
            if line[7].strip(' ')[-1]=='E':
                lon_ALL.append(float(line[7].strip(' ')[:-1])/10.)
            elif line[7].strip(' ')[-1]=='W':
                lon_ALL.append(-float(line[7].strip(' ')[:-1])/10.)
            else:
                lon_ALL.append(np.nan)

            int_ALL.append(int(line[8].strip(' ')))



    dtg_ALL     = np.array(dtg_ALL)
    model_ALL   = np.array(model_ALL)
    fhr_ALL     = np.array(fhr_ALL)
    lat_ALL     = np.array(lat_ALL)
    lon_ALL     = np.array(lon_ALL)
    int_ALL     = np.array(int_ALL)
    

    if stormtime:
        return dtg_ALL[np.isin(dtg_ALL,stormtime)], model_ALL[np.isin(dtg_ALL,stormtime)], fhr_ALL[np.isin(dtg_ALL,stormtime)], lat_ALL[np.isin(dtg_ALL,stormtime)], lon_ALL[np.isin(dtg_ALL,stormtime)], int_ALL[np.isin(dtg_ALL,stormtime)]


    else:
        return dtg_ALL, model_ALL, fhr_ALL, lat_ALL, lon_ALL, int_ALL
    

def get_all_ens(file_path, stormtime=False):
    dtg_0, model_0, fhr_0, lat_0, lon_0, int_0 = get_ukme(file_path,stormtime)
    dtg_1, model_1, fhr_1, lat_1, lon_1, int_1 = get_gefs(file_path,stormtime)
    dtg_2, model_2, fhr_2, lat_2, lon_2, int_2 = get_eps(file_path,stormtime)

    
    dtg_c = np.concatenate((dtg_0,dtg_1,dtg_2))
    model_c = np.concatenate((model_0,model_1,model_2))
    fhr_c = np.concatenate((fhr_0,fhr_1,fhr_2))
    lat_c = np.concatenate((lat_0,lat_1,lat_2))
    lon_c = np.concatenate((lon_0,lon_1,lon_2))
    int_c = np.concatenate((int_0,int_1,int_2))


    return dtg_c, model_c, fhr_c, lat_c, lon_c, int_c 
  
    
def get_ens_interped_asarray(full_path,dttoplot=False, return_latest=False):
    #
    # This function will download all the adeck ensemble members for gefs and ukmet
    #
    #
    
    #Get the datetimes, modelnames, forecast houres, lats, lons, and intensities
#    dtg_, model_, fhr_, lat_, lon_, int_ = get_gefs(full_path)    
    dtg_, model_, fhr_, lat_, lon_, int_ = get_all_ens(full_path)    

    print('starting interpolation')
    print(model_,lat_)

    
    newt = np.arange(0,126,6)
    times_all = np.unique(dtg_)
    model_all = np.unique(model_)
    
    if return_latest!=False:
        dttoplot = sorted(times_all)[-1]
    
    
    if dttoplot:
        #initialize arrays for lat/lon and intensty
        lats_all = np.full((len(model_all),len(newt)),np.nan,dtype=float)
        lons_all = np.full((len(model_all),len(newt)),np.nan,dtype=float)
        ints_all = np.full((len(model_all),len(newt)),np.nan,dtype=float)

        for i,modeln in enumerate(model_all):
            lo_ = lon_[((model_==modeln)&(dtg_==dttoplot))]
            la_ = lat_[((model_==modeln)&(dtg_==dttoplot))]
            c_s = int_[(model_==modeln)&(dtg_==dttoplot)]
            f_s = fhr_[(model_==modeln)&(dtg_==dttoplot)]     

            if len(lo_)==0: #There was no forecast for that ensemble member
                continue

            lats = np.interp(newt,f_s,la_,left=np.nan,right=np.nan)
            lons = np.interp(newt,f_s,lo_,left=np.nan,right=np.nan)
            ints = np.interp(newt,f_s,c_s,left=np.nan,right=np.nan)

            lats_all[i,:]=lats
            lons_all[i,:]=lons
            ints_all[i,:]=ints
            
    else: #If we want all of the times then 
        #initialize arrays for lat/lon and intensty
        lats_all = np.full((len(times_all),len(model_all),len(newt)),np.nan,dtype=float)
        lons_all = np.full((len(times_all),len(model_all),len(newt)),np.nan,dtype=float)
        ints_all = np.full((len(times_all),len(model_all),len(newt)),np.nan,dtype=float)

        #No time was specified, so loop through all of them
        for tt,dttoplot in enumerate(times_all):
            for i,modeln in enumerate(model_all):
                lo_ = lon_[((model_==modeln)&(dtg_==dttoplot))]
                la_ = lat_[((model_==modeln)&(dtg_==dttoplot))]
                c_s = int_[(model_==modeln)&(dtg_==dttoplot)]
                f_s = fhr_[(model_==modeln)&(dtg_==dttoplot)]     

                if len(lo_)==0: #There was no forecast for that ensemble member
                    continue

                lats = np.interp(newt,f_s,la_,left=np.nan,right=np.nan)
                lons = np.interp(newt,f_s,lo_,left=np.nan,right=np.nan)
                ints = np.interp(newt,f_s,c_s,left=np.nan,right=np.nan)

                lats_all[tt,i,:]=lats
                lons_all[tt,i,:]=lons
                ints_all[tt,i,:]=ints

    return lats_all, lons_all, ints_all, times_all, model_all

        



#------------------#
def define_ens_map(nclusters,lon_limits=None):
    #This function creates the figure and addes the background map


    HI_RES = '50m'
    MED_RES = '50m'
    PROJ = cartopy.crs.PlateCarree()

    map_proj = ccrs.PlateCarree(central_longitude=np.mean(lon_limits))

    fig, ax = plt.subplots(
        figsize=(12, 8),
        dpi=400,
        constrained_layout=False,
        facecolor='w',
        edgecolor='k',
        subplot_kw={
            'projection': map_proj,
        },
    )

    lands = cartopy.feature.NaturalEarthFeature('physical', 'land', HI_RES)
    land_df = pd.DataFrame([[land.area, land] for land in lands.geometries()])
    nlands = len(land_df)
    land_shapes = land_df.sort_values(by=0, axis=0).tail(int(nlands * 0.8))[1]
    lakes = cartopy.feature.NaturalEarthFeature('physical', 'lakes', HI_RES)
    lake_df = pd.DataFrame([[lake.area, lake] for lake in lakes.geometries()])
    lake_shapes = lake_df.sort_values(by=0, axis=0).tail(25)[1]

    # Draw a grey base layer under the continents with the black edge
    great_land_hatching = cartopy.feature.ShapelyFeature(
        land_shapes,
        crs=PROJ,
        edgecolor='k',
        linewidth=1.,
        facecolor=(0.7, 0.7, 0.7, 0.6),
        zorder=-6,
    )
    ax.add_feature(great_land_hatching)
    great_lands_edges = cartopy.feature.ShapelyFeature(
        land_shapes,
        crs=PROJ,
        edgecolor='k',
        linewidth=1.,
        facecolor='None',
        zorder=-1,
    )
    ax.add_feature(great_lands_edges)
    land_shapes = [
        shape.buffer(-0.35) for shape in great_lands_edges.geometries()
    ]
    # Draw the interior white fill with a semi transparent edge
    great_land_interior = cartopy.feature.ShapelyFeature(
        land_shapes,
        crs=PROJ,
        edgecolor=(0.9, 0.9, 0.9, 0.7),
        linewidth=5,
        facecolor=(1.0, 1.0, 1.0, 0.9),
        zorder=-5,
    )
    ax.add_feature(great_land_interior)
    great_lakes = cartopy.feature.ShapelyFeature(
        lake_shapes,
        crs=PROJ,
        edgecolor='None',
        linewidth=0,
        facecolor='None',
        zorder=-4,
    )
    ax.add_feature(great_lakes)
    countries = cartopy.feature.NaturalEarthFeature(
        'cultural',
        'admin_0_boundary_lines_land',
        MED_RES,
        edgecolor='k',
        linewidth=1,
        facecolor='None',
        zorder=-2,
    )
    ax.add_feature(countries)
    borders = cartopy.feature.NaturalEarthFeature(
        'cultural',
        'admin_1_states_provinces_lines',
        MED_RES,
        edgecolor='k',
        linewidth=0.3,
        facecolor='None',
        zorder=-2,
    )
    ax.add_feature(borders)



    gs = gridspec.GridSpec(3,nclusters)
    ax.set_position(gs[0:2,:].get_position(fig))

    axes = []
    for nc in range(0,nclusters):
        exec('ax%i = fig.add_subplot(gs[2,%i])'%(nc,nc))
        exec('axes.append(ax%i)'%(nc))
    

    #ADD teh grid lines for lat lon
    gl = ax.gridlines(crs=ccrs.PlateCarree(), draw_labels=True,
                  linewidth=1, color=(0.7, 0.7, 0.7, 0.5), alpha=0.5, linestyle='--',zorder=-3)

    #Specify where to put lat/lon grid lines
    gl.xlocator = mticker.FixedLocator(np.arange(-145,5,5))
    gl.ylocator = mticker.FixedLocator([0,5,10,15,20,25,30,35,40,45,50,55,60])

    #Dont label the right y axis or top of plot
    gl.xlabels_top = False
    gl.ylabels_right = False

    #Make the lat/lon format with E W and N S
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER


    return fig,PROJ, ax, axes



def format_vsub(ax_,nclust):
    #Adds formatting to subplot for intensity distributions
    ax_.set_yticks(np.arange(0,180,20))

    #Alternating shading of saffir simpson wind hurricane scale
    ax_.fill_between(np.arange(0,300,10),0,34,color='silver',zorder=1,alpha=.3)
    ax_.fill_between(np.arange(0,300,10),64,83,color='silver',zorder=1,alpha=.3)
    ax_.fill_between(np.arange(0,300,10),96,113,color='silver',zorder=1,alpha=.3)
    ax_.fill_between(np.arange(0,300,10),137,190,color='silver',zorder=1,alpha=.3)

    if nclust<3:
        ax_.set_ylabel('Intensity (kt)',fontsize=11)

    if nclust>4:
        ax_.set_xticks(np.arange(0,180,36))
        ax_.tick_params(axis='y',labelsize=8.5)
    else:
        ax_.set_xticks(np.arange(0,192,24))
        
    

    ax_.set_xlabel('Forecast Length (h)',fontsize=11)


    #Max intensity is set at 160 kt (may need to change)
    ax_.set_ylim(25,160)

    
def get_best(file_path,dttoref=None):
    '''
    This code will grab all the best track data that are available in the bdeck file
    The output will be the formatted data for the storm

    input atcf path with storm name attached to end
    input single datetime string to put into hours since

    output numpy arrays of
    datetime YYYYMMDDHH as a string
    forecast hour as int
    lat in deg as float
    lon in deg as float
    intensity in kt as int


   '''

    dtg_ALL      = []
    fhr_ALL      = []
    lat_ALL      = []
    lon_ALL      = []
    int_ALL      = []


    #This will pull all the forecast data and remove any 50 or 64 kt wind duplicates
    data_list        = str(subprocess.check_output(("grep " + "BEST "+file_path+" | awk -F', ' '{ if(substr($12,length($12)-1,2)!=\"50\" && substr($12,length($12)-1,2)!=\"64\") print}' | sort -u"),shell=True).decode('utf8')).splitlines()

    for data in data_list:
        line = data.split(',')
        #Add the data to the empty list
        dtg_ALL.append(line[2].strip(' '))
        fhr_ALL.append(int(line[5].strip(' ')))

        #Check which hemisphere the latitude is to format as a float
        if line[6].strip(' ')[-1]=='N':
            lat_ALL.append(float(line[6].strip(' ')[:-1])/10.)
        elif line[6].strip(' ')[-1]=='S':
            lat_ALL.append(-float(line[6].strip(' ')[:-1])/10.)
        else:
            lat_ALL.append(np.nan)

        #Now check which hemisphere the longitude is to format as float
        if line[7].strip(' ')[-1]=='E':
            lon_ALL.append(float(line[7].strip(' ')[:-1])/10.)
        elif line[7].strip(' ')[-1]=='W':
            lon_ALL.append(-float(line[7].strip(' ')[:-1])/10.)
        else:
            lon_ALL.append(np.nan)

        int_ALL.append(int(line[8].strip(' ')))
        


    dtg_ALL     = np.array(dtg_ALL)
    fhr_ALL     = np.array(fhr_ALL)
    lat_ALL     = np.array(lat_ALL)
    lon_ALL     = np.array(lon_ALL)
    int_ALL     = np.array(int_ALL)
    
    if dttoref!=None:
        init_dt = dt.datetime.strptime(dttoref,'%Y%m%d%H')

        for idx,dtg_ in enumerate(dtg_ALL):
            dtg_v = dt.datetime.strptime(dtg_,'%Y%m%d%H')
            fhr_ALL[idx] = int((dtg_v-init_dt).total_seconds()/(60.*60.))
 

    return dtg_ALL, fhr_ALL, lat_ALL, lon_ALL, int_ALL


def run_bmm(lons_all,lats_all,nclust):

    POINTS = np.array([lons_all,lats_all])
    NPOINTS = (np.moveaxis(POINTS,0,2)).reshape(POINTS.shape[1],-1)
    
    #If there are nans, then the mixture model won't run
    NPOINTS[np.isnan(NPOINTS)]=2000

    bgm = BayesianGaussianMixture(n_components=nclust,max_iter=99999,tol=.00001,random_state=42,weight_concentration_prior_type='dirichlet_distribution').fit(NPOINTS)
    
    return bgm.predict(NPOINTS),bgm


def eval_bmm(lons_all,lats_all,model_all,nclust):

    POINTS = np.array([lons_all,lats_all])
    NPOINTS = (np.moveaxis(POINTS,0,2)).reshape(POINTS.shape[1],-1)
    
    #If there are nans, then the mixture model won't run
    NPOINTS[np.isnan(NPOINTS)]=3000

    bgm = BayesianGaussianMixture(n_components=nclust,max_iter=99999,tol=.00001,random_state=42,weight_concentration_prior_type='dirichlet_distribution').fit(NPOINTS)

    mod_c = bgm.predict(NPOINTS)
    
    total_model  = lons_all.shape[0]
    total_finter = lons_all.shape[1]
    
    TRUE_Cval = np.copy(mod_c)
    
    
    for i2,ensmn in enumerate(np.unique(mod_c)):
        missing_data = np.nansum(np.isnan(lons_all.T[:,mod_c==ensmn]),axis=0)
        nmodels      = lons_all.T[:,mod_c==ensmn].shape[1]
        
        
        #If the clusters have too small amount of data or too much missing data (i.e. failed genesis)
        if (((nmodels/total_model)<=.05)|((np.nanmean(missing_data)/total_finter)>=.70)):
            TRUE_Cval[mod_c==ensmn]=99
        
        #If the fractional component is near 90% then we have too many values in a single cluster
        if ((float(nmodels)/total_model)>=.8):
            lons_all2 = lons_all.T[:,mod_c==ensmn].T
            lats_all2 = lats_all.T[:,mod_c==ensmn].T

            POINTS2 = np.array([lons_all2,lats_all2])
            NPOINTS2 = (np.moveaxis(POINTS2,0,2)).reshape(POINTS2.shape[1],-1)

            #If there are nans, then the mixture model won't run
            NPOINTS2[np.isnan(NPOINTS2)]=3000

            bgm2 = BayesianGaussianMixture(n_components=3,max_iter=99999,tol=.00001,random_state=42,weight_concentration_prior_type='dirichlet_distribution').fit(NPOINTS2)

            mod_c2 = np.array(bgm2.predict(NPOINTS2))+(nclust)
            nclust =np.nanmax(mod_c2)
            
            for i3,ensmn2 in enumerate(np.unique(mod_c2)):                
#                TRUE_Cval[mod_c==ensmn][mod_c2==ensmn2][:]=ensmn2

                tempdat = TRUE_Cval[np.array(mod_c)==(ensmn)]
                
                tempdat[np.array(mod_c2)==(ensmn2)]=ensmn2
                
                TRUE_Cval[np.array(mod_c)==(ensmn)]=tempdat
              


    mod_cs  = []
    arsize  = []
                
    for i,ensmn in enumerate(np.unique(TRUE_Cval)):            
        mod_cs.append(ensmn)
        arsize.append(lons_all.T[:,TRUE_Cval==ensmn].shape[1])

    
    mod_csort = (np.array(mod_cs)[np.argsort(arsize)])[::-1]  
    

    new_clust = len(np.unique(TRUE_Cval))

    return TRUE_Cval,bgm, mod_csort,new_clust



def interp_int(fh_arr,int_arr,best_int,offstart,offend):
    
    new_int_arr = np.copy(int_arr)
    
    offset = float(best_int-int_arr[fh_arr==0])
    
    for i,fh in enumerate(fh_arr):
        if fh<=offstart:
            new_int_arr[i] = int_arr[i]+(offset)
            
        elif fh>offend:
            new_int_arr[i] = int_arr[i]
            
        else:
            new_int_arr[i] = int_arr[i] + (offset)*(1.-((fh-offstart)/(offend-offstart)))
            
    return new_int_arr







def plot_BMM_ens(pathtoadeck,pathtobdeck,nclust,dttoplot=False, return_latest=False,tlim=168,offset=False):
    props=dict(facecolor='w',alpha=1,edgecolor='k')

    ensdict = {'AP':'GEFS','UE':'UKMO','EN':'EPS','EE':'EPS','EP':'EPS'}
#    ensdict = {'AP':'GEFS'}

    stormname = pathtobdeck.split('/')[-1][1:-4].upper()

 
    # Read in the data from the adecks
    lats_all, lons_all, ints_all, times_all, model_all = get_ens_interped_asarray(pathtoadeck,dttoplot,return_latest)

    new_lons = []
    new_lats = []
    new_ints = []
    new_mn = []


    for i in range(0,lons_all.shape[0]):
        if (all(np.isnan(lons_all[i,1:])))|(all(np.isnan(lats_all[i,1:]))):
            continue
        else:
            new_lons.append(lons_all[i])
            new_lats.append(lats_all[i])            
            new_ints.append(ints_all[i])            
            new_mn.append(model_all[i])

    new_lons = np.array(new_lons)
    new_lats = np.array(new_lats)
    new_ints = np.array(new_ints)
    new_mods = np.array(new_mn)
    ensmn_all = np.array([val[:2] for val in new_mn])

    
    mod_c,bgm,mod_csort,nclust = eval_bmm(new_lons,new_lats,new_mods,nclust)
    
    
    ftimes = np.arange(0,126,6)
    
        
    color_list2 = ['seagreen','red','royalblue','orange','darkviolet','gold','sienna','darkgrey']
    color_list3 = ['darkgreen','darkred','blue','darkorange','indigo','goldenrod','saddlebrown','dimgray']

    
    if (dttoplot!=False) | (return_latest!=False):
        TOTAL_ENS_N = lons_all.shape[0]
        
        bfdtg, bfh, blat,blon,bint = get_best(pathtobdeck,dttoplot) #Get the best track

        init_vmax = bint[np.array(bfh)==0.]
        
        if offset==True:
            new_ints2 = np.copy(new_ints)

            for ip in range(0,new_ints2.shape[0]):
                
                new_ints2[ip] = interp_int(ftimes,new_ints2[ip],init_vmax,12,120)
            new_ints = new_ints2    
            #new_ints = new_ints +(init_vmax-new_ints[:,0].reshape((new_ints.shape[0],1)))
        
        
        fig,PROJ, ax, axes = define_ens_map(nclust,[0.1,359.9])
        ax.plot(blon,blat,c='k',lw=3,alpha=1,transform=PROJ,zorder=9999)
        
        


        for i2,ensmn in enumerate(mod_csort):

            ax.plot(new_lons.T[:,mod_c==ensmn][ftimes<=tlim],new_lats.T[:,mod_c==ensmn][ftimes<=tlim],c=color_list2[i2],lw=2,alpha=.3,transform=PROJ,zorder=99999)
            ax.plot(np.nanmean(new_lons.T[:,mod_c==ensmn][ftimes<=tlim],axis=1),np.nanmean(new_lats.T[:,mod_c==ensmn][ftimes<=tlim],axis=1),c=color_list3[i2],lw=3,alpha=1,transform=PROJ,zorder=999999999,label='Group %s mean'%i2)
            ax.scatter(new_lons.T[:,mod_c==ensmn][ftimes<=tlim][-1],new_lats.T[:,mod_c==ensmn][ftimes<=tlim][-1],color=color_list2[i2],alpha=.8,transform=PROJ,zorder=99999999)
            ax.scatter(np.nanmean(new_lons.T[:,mod_c==ensmn][ftimes<=tlim],axis=1)[-1],np.nanmean(new_lats.T[:,mod_c==ensmn][ftimes<=tlim],axis=1)[-1],color=color_list3[i2],marker='*',s=150,edgecolor='k',alpha=1,transform=PROJ,zorder=99999999999999)


            axes[i2].plot(ftimes[ftimes<=tlim],(new_ints.T[:,mod_c==ensmn][ftimes<=tlim]),c=color_list2[i2],lw=2,alpha=.4,zorder=999999999)
            axes[i2].plot(ftimes[ftimes<=tlim],np.nanmean((new_ints.T[:,mod_c==ensmn][ftimes<=tlim]),axis=1),c=color_list3[i2],lw=3,alpha=1,zorder=999999999,label=ensmn)
            axes[i2].scatter(ftimes[ftimes<=tlim][-1],np.nanmean((new_ints.T[:,mod_c==ensmn][ftimes<=tlim]),axis=1)[-1],color=color_list3[i2],marker='*',s=150,edgecolor='k',alpha=1,zorder=99999999999)


                
            axes[i2].plot(bfh,bint,c='k',lw=3,alpha=1,zorder=9999,label='BEST')

          
            axes[i2].set_xlim(ftimes[0],ftimes[-1])
            format_vsub(axes[i2],nclust)

#            ax.plot([],[],c=color_list2[i2],lw=2,alpha=.9,zorder=999999,label='Group %s members'%ensmn)

            axes[i2].axvline(tlim,lw=2,ls='dotted',alpha=.7,c='k')


            axes[i2].set_title('Group %i, %i Members'%(i2,len(new_lons[mod_c==ensmn])),fontsize=10,y=.97,color='k',zorder=10)


        ax.set_title('   %s     Init: %s       Valid: %s (%sh)      Total Ens: %s      '%(stormname,dt.datetime.strptime(dttoplot,'%Y%m%d%H').strftime('%HZ %b %d %Y'),(dt.datetime.strptime(dttoplot,'%Y%m%d%H')+dt.timedelta(hours=int(tlim))).strftime('%HZ %b %d %Y'),tlim,TOTAL_ENS_N),fontsize=14,color='k')
        ax.text(.02,.95,'Models Included:\n%s'%('\n'.join([ensdict[model_] for model_ in np.unique(ensmn_all)])),fontsize=11,color='k',ha='left',va='top',bbox=props,transform=ax.transAxes,zorder=9999999999999999999)



        
        if nclust>=3:
            axes[0].set_ylabel('Intensity (kt)',fontsize=12)


        llclon=np.nanmax([roundD(np.nanmin(new_lons)-10),-105])
        llclat=np.nanmax([roundD(np.nanmin(new_lats)-1),4])
        urclon=np.nanmin([roundU(np.nanmax(new_lons)+10),-20])
        urclat=np.nanmin([roundU(np.nanmax(new_lats)+1),54])
        
        if (np.abs(urclat-llclat)/np.abs(urclon-llclon))>.7:
            ax.legend(loc='upper left', bbox_to_anchor=(1, 1),framealpha=1,prop={'size':11})

        else:
            ax.legend(loc='upper right', bbox_to_anchor=(1, 1),framealpha=1,prop={'size':11})

            

        #Set the boundaries of the map
        ax.set_extent([llclon, urclon, llclat, urclat], crs=ccrs.PlateCarree())
        plt.savefig('../figures/BGMM_%s_%s_%03d.png'%(stormname.upper(),dttoplot,tlim),bbox_inches='tight')
        plt.close(fig)


#        plt.show()
        
    else:
        TOTAL_ENS_N = lons_all.shape[1]

        for idt,dttoplot in enumerate(times_all):

            mod_c,bgm = run_bmm(lons_all[idt],lats_all[idt],nclust)


            bfdtg, bfh, blat,blon,bint = get_best(pathtobdeck,dttoplot)

            if offset==True:
                new_ints[idt] = new_ints[idt] +(init_vmax-new_ints[idt][:,0].reshape((new_ints[idt].shape[0],1)))


            fig,PROJ, ax, axes = define_ens_map(nclust,[0.1,359.9])

            ax.plot(blon,blat,c='k',lw=3,alpha=1,transform=PROJ,zorder=99999999,label='BEST')


            for i2,ensmn in enumerate(np.unique(mod_c)):
                
                ax.plot(lons_all[idt].T[:,mod_c==ensmn][ftimes<=tlim],lats_all[idt].T[:,mod_c==ensmn][ftimes<=tlim],c=color_list2[i2],lw=2,alpha=.3,transform=PROJ,zorder=999999)
                ax.plot(np.nanmean(lons_all[idt].T[:,mod_c==ensmn][ftimes<=tlim],axis=1),np.nanmean(lats_all[idt].T[:,mod_c==ensmn][ftimes<=tlim],axis=1),c=color_list3[i2],lw=3,alpha=1,transform=PROJ,zorder=999999999,label='Group  %s mean'%i2)
                ax.scatter(lons_all[idt].T[:,mod_c==ensmn][ftimes<=tlim][-1],lats_all[idt].T[:,mod_c==ensmn][ftimes<=tlim][-1],color=color_list2[i2],alpha=.8,transform=PROJ,zorder=999999999)
                ax.scatter(np.nanmean(lons_all[idt].T[:,mod_c==ensmn][ftimes<=tlim],axis=1)[-1],np.nanmean(lats_all[idt].T[:,mod_c==ensmn][ftimes<=tlim],axis=1)[-1],color=color_list3[i2],marker='*',s=150,edgecolor='k',alpha=1,transform=PROJ,zorder=99999999999999)



                axes[i2].plot(ftimes[ftimes<=tlim],(ints_all[idt].T[:,mod_c==ensmn][ftimes<=tlim]),c=color_list2[i2],lw=2,alpha=.4,zorder=999999999)
                axes[i2].plot(ftimes[ftimes<=tlim],np.nanmean((ints_all[idt].T[:,mod_c==ensmn][ftimes<=tlim]),axis=1),c=color_list3[i2],lw=3,alpha=1,zorder=999999999,label=ensmn)
                axes[i2].scatter(ftimes[ftimes<=tlim][-1],np.nanmean((ints_all[idt].T[:,mod_c==ensmn][ftimes<=tlim]),axis=1)[-1],color=color_list3[i2],marker='*',s=150,edgecolor='k',alpha=1,zorder=99999999999)


                axes[i2].set_xlim(ftimes[0],ftimes[-1])
                format_vsub(axes[i2],nclust)

                ax.plot([],[],c=color_list2[i2],lw=2,alpha=.9,zorder=1,label='Group %s members'%i2)

                axes[i2].plot(bfh,bint,c='k',lw=3,alpha=1,zorder=9999999,label='BEST')


                axes[i2].axvline(tlim,lw=2,ls='dotted',alpha=.7,c='k')



                axes[i2].set_title('Group %i, %i Members'%(i2,len(lons_all[idt][mod_c==ensmn])),fontsize=10,color='k',zorder=10)


            ax.set_title('   %s     Init: %s       Valid: %s (%sh)       Total Ens %s      '%(stormname,dt.datetime.strptime(dttoplot,'%Y%m%d%H').strftime('%HZ %b %d %Y'),(dt.datetime.strptime(dttoplot,'%Y%m%d%H')+dt.timedelta(hours=int(tlim))).strftime('%HZ %b %d %Y'),tlim,TOTAL_ENS_N),fontsize=14,color='k')
            ax.text(.02,.95,'Models Included:\n%s'%('\n'.join([ensdict[model_] for model_ in np.unique(ensmn_all)])),fontsize=11,color='k',ha='left',va='top',bbox=props,transform=ax.transAxes,zorder=9999999999999999999)


            if nclust>=3:
                axes[0].set_ylabel('Intensity (kt)',fontsize=12)


            llclon=np.nanmax([roundD(np.nanmin(lons_all[idt])-10),-105])
            llclat=np.nanmax([roundD(np.nanmin(lats_all[idt])-1),4])
            urclon=np.nanmin([roundU(np.nanmax(lons_all[idt])+10),-20])
            urclat=np.nanmin([roundU(np.nanmax(lats_all[idt])+1),54])

            #Set the boundaries of the map
            ax.set_extent([llclon, urclon, llclat, urclat], crs=ccrs.PlateCarree())

            if (np.abs(urclat-llclat)/np.abs(urclon-llclon))>.7:
                ax.legend(loc='upper left', bbox_to_anchor=(1, 1),framealpha=1,prop={'size':11})

            else:
                ax.legend(loc='upper right', bbox_to_anchor=(1, 1),framealpha=1,prop={'size':11})


            
            
            plt.savefig('../figures/BGMM_%s_%s_%03d.png'%(stormname.upper(),dttoplot,tlim),bbox_inches='tight')
            plt.close(fig)
#            plt.show()


def plot_BMM_ens_bc(full_path,nclust,dttoplot=False, return_latest=False,bcorrection=None):
    lats_all, lons_all, ints_all, times_all, model_all = get_ens_interped_asarray(adeck_path+'/a'+stormname+'.dat',dttoplot,return_latest)
    ensmn_all = np.array([val[:2] for val in model_all])
    
    mod_c,bgm = run_bmm(lons_all,lats_all,nclust)
    
    
    
    BC_matrix,SD_matrix,N,MAE_matrix = get_BC('qkver_2015-2019_Bal_AP.dat')
   
    
    
    ftimes = np.arange(0,174,6)
    
        
    color_list2 = ['seagreen','red','royalblue','orange','darkviolet']
    color_list3 = ['darkgreen','darkred','blue','darkorange','indigo']

    
    if (dttoplot!=False) | (return_latest!=False):
        bfdtg, bfh, blat,blon,bint = get_best(full_path.replace('aal','bal'),dttoplot)

        init_vmax = bint[np.array(bfh)==0.]
        
        
        ints_all = ints_all +(init_vmax-ints_all[:,0].reshape((ints_all.shape[0],1)))
        
        
        fig,PROJ, ax, axes = define_ens_map(nclust,[0.1,359.9])
        ax.plot(blon,blat,c='k',lw=3,alpha=1,transform=PROJ,zorder=9999999999999999)


        for i2,ensmn in enumerate(np.unique(mod_c)):
            ax.plot(lons_all.T[:,mod_c==ensmn],lats_all.T[:,mod_c==ensmn],c=color_list2[i2],lw=2,alpha=.3,transform=PROJ,zorder=999999999)
            ax.plot(np.nanmean(lons_all.T[:,mod_c==ensmn],axis=1),np.nanmean(lats_all.T[:,mod_c==ensmn],axis=1),c=color_list3[i2],lw=3,alpha=1,transform=PROJ,zorder=999999999,label='Group %s mean'%ensmn)

            if bcorrection=='Amplify':
                axes[i2].plot(ftimes,apply_amplification(ints_all.T[:,mod_c==ensmn]),c=color_list2[i2],lw=2,alpha=.4,zorder=999999999)
                axes[i2].plot(ftimes,np.nanmean(apply_amplification(ints_all.T[:,mod_c==ensmn]),axis=1),c=color_list3[i2],lw=3,alpha=1,zorder=999999999,label=ensmn)

            elif bcorrection=='IBUS':
                all_new = []

                for intfor in ints_all.T[:,mod_c==ensmn].T:
                    newt_,newf_ = Apply_gefs_BC(BC_matrix,np.array(ftimes),intfor)
                    axes[i2].plot(newt_,newf_,c=color_list2[i2],lw=2,alpha=.4,zorder=999999999)
                    all_new.append(newf_)

                axes[i2].plot(newt_,np.nanmean(np.array(all_new),axis=0),c=color_list3[i2],lw=3,alpha=1,zorder=999999999,label=ensmn)

            else:
                axes[i2].plot(ftimes,(ints_all.T[:,mod_c==ensmn]),c=color_list2[i2],lw=2,alpha=.4,zorder=999999999)
                axes[i2].plot(ftimes,np.nanmean((ints_all.T[:,mod_c==ensmn]),axis=1),c=color_list3[i2],lw=3,alpha=1,zorder=999999999,label=ensmn)

                
            axes[i2].plot(bfh,bint,c='k',lw=3,alpha=1,zorder=9999999999999999,label='BEST')


            
            
            axes[i2].set_xlim(ftimes[0],ftimes[-1])
            format_vsub(axes[i2],nclust)

            ax.plot([],[],c=color_list2[i2],lw=2,alpha=.3,zorder=999999999,label='Group %s members'%ensmn)


        ax.legend()
        
        if nclust>=3:
            axes[0].set_ylabel('Intensity (kt)',fontsize=12)


        llclon=np.nanmax([roundD(np.nanmin(lons_all)-10),-105])
        llclat=np.nanmax([roundD(np.nanmin(lats_all)-1),4])
        urclon=np.nanmin([roundU(np.nanmax(lons_all)+10),-20])
        urclat=np.nanmin([roundU(np.nanmax(lats_all)+1),54])

        #Set the boundaries of the map
        ax.set_extent([llclon, urclon, llclat, urclat], crs=ccrs.PlateCarree())

        plt.show()
        
    else:
        for idt,dttoplot in enumerate(times_all):

            fig,PROJ, ax, axes = define_ens_map(nclust,[0.1,359.9])

            for i2,ensmn in enumerate(np.unique(ensmn_all)):
                ax.plot(lons_all[idt].T[:,ensmn_all==ensmn],lats_all[idt].T[:,ensmn_all==ensmn],c=color_list2[i2],lw=2,alpha=.3,transform=PROJ,zorder=999999999)
                ax.plot(np.nanmean(lons_all[idt].T[:,ensmn_all==ensmn],axis=1),np.nanmean(lats_all[idt].T[:,ensmn_all==ensmn],axis=1),c=color_list3[i2],lw=3,alpha=1,transform=PROJ,zorder=999999999,label='%s mean'%ensmn)

                axes[i2].plot(ftimes,apply_amplification(ints_all[idt].T[:,ensmn_all==ensmn]),c=color_list2[i2],lw=2,alpha=.4,zorder=999999999)
                axes[i2].plot(ftimes,np.nanmean(apply_amplification(ints_all[idt].T[:,ensmn_all==ensmn]),axis=1),c=color_list3[i2],lw=3,alpha=1,zorder=999999999,label=ensmn)

                axes[i2].set_xlim(ftimes[0],ftimes[-1])
                format_vsub(axes[i2])

                ax.plot([],[],c=color_list2[i2],lw=2,alpha=.3,zorder=999999999,label='%s members'%ensmn)


            ax.legend()


            llclon=np.nanmax([roundD(np.nanmin(lons_all[idt])-10),-105])
            llclat=np.nanmax([roundD(np.nanmin(lats_all[idt])-1),4])
            urclon=np.nanmin([roundU(np.nanmax(lons_all[idt])+10),-20])
            urclat=np.nanmin([roundU(np.nanmax(lats_all[idt])+1),54])

            #Set the boundaries of the map
            ax.set_extent([llclon, urclon, llclat, urclat], crs=ccrs.PlateCarree())


            plt.show()






if __name__ == '__main__':

    for stormname,inittime in zip(STORMNAMES,INITTIMES):
        for FLIM in range(24,155,24):
            plot_BMM_ens('../data/aid/a%s.dat'%stormname,'../data/btk/b%s.dat'%stormname,5,inittime,tlim=FLIM,offset=False)


