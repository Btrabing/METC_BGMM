'''

Please note that the bias correction functions used within this script are purely developmental and have not been tested thoroughly

More work needs to be done to use interpolated versions of the ensemble and then to apply a bias correction scheme customized to each ensemble system


'''

import numpy as np
from sklearn.mixture import BayesianGaussianMixture
import pandas as pd
import matplotlib.pyplot as plt
import datetime as dt
import matplotlib as mpl
import matplotlib.gridspec as gridspec
import cartopy
import cartopy.crs as ccrs
import cartopy.mpl.ticker
from descartes import PolygonPatch
from matplotlib.collections import LineCollection
import glob
import matplotlib.patheffects as PathEffects
import matplotlib.text as mtext
import matplotlib.ticker as mticker
import matplotlib.transforms as mtrans
from matplotlib.colors import ListedColormap
import matplotlib.patches as mpatches
from cartopy.mpl import gridliner
from matplotlib.patches import Ellipse
from shapely.geometry import Polygon
from shapely.geometry import Point
from shapely.ops import unary_union
import cartopy.io.shapereader as shpreader
import cartopy.feature as cfeature
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
#import geopandas
import xarray as xr

import warnings
warnings.filterwarnings("ignore")

import subprocess
import sys
#----------------------------














def roundD(x, base=5):
    return base * np.floor(x/base)

def roundU(x, base=5):
    return base * np.ceil(x/base)


def apply_amplification_2nd(intensity):
    new_intensity = np.copy(intensity)
    
    intensity_change = intensity[1:]-intensity[:-1]
    intensity_change[intensity_change<=-2]= 1.5*intensity_change[intensity_change<=-2]
    intensity_change[intensity_change>=2]= 3.*intensity_change[intensity_change>=2]
    
    new_intensity[1:] = intensity_change+intensity[0]
    
    return new_intensity

def apply_amplification(intensity):
    new_intensity = np.copy(intensity)
    
    intensity_change = intensity[1:]-intensity[0]
    intensity_change[intensity_change<=-3]= 1.5*intensity_change[intensity_change<=-3]
    intensity_change[intensity_change>=3]= 2.5*intensity_change[intensity_change>=3]
    
    new_intensity[1:] = intensity_change+intensity[0]
    
    return new_intensity


## IBUS Functions
#----------------------------------


def Apply_gefs_BC(BC_matrix,FHOUR,VINT):
    F_name = np.arange(0,180,12)

    vmins = np.arange(-127.5, 127.5,5)
    vmaxs = np.arange(-122.5,132.5,5)

    new_intensity = np.copy(F_name)
    new_intensity[:] = VINT[0]
    
    intensity_change = VINT[1:]-VINT[0]
        
    new_intensity_change2 = np.copy(new_intensity[1:])
    

    i2=0
    for i,fh in enumerate(FHOUR[1:]):

        
        INDEX = (np.argwhere(F_name==float(fh)))

        if INDEX:

            j=0       
            for vmin,vmax in zip(vmins,vmaxs):
                if (j==0)&(intensity_change[i]<vmin):
                    #If the change is less than the vmin bounds make it the edge
                    new_intensity_change2[i2] = intensity_change[i]-(BC_matrix[int(INDEX)-1,j])
                    i2+=1
                    break

                if (j==len(vmins))&(intensity_change[i]>vmax):
                    #If the change is less than the vmin bounds make it the edge
                    new_intensity_change2[i2] = intensity_change[i]-(BC_matrix[int(INDEX)-1,j])
                    i2+=1
                    break

                if (intensity_change[i]<vmax)&(intensity_change[i]>vmin):
                    new_intensity_change2[i2] = intensity_change[i]-(BC_matrix[int(INDEX)-1,j])
                    i2+=1
                    break



                j+=1

    new_intensity[1:] = new_intensity_change2+VINT[0]

    return F_name,new_intensity



def myround(x, base=5):
    return base * np.round(x/base)


def read_vmod(filename):
    '''
    Name = 0
    Datetime = 1
    V(0) = 2
    Lat = 3
    Lon = 4
    Best Track Intensity Change = 5
    OFCL = 6
    '''
    with open(filename) as f:
        f12= []
        f24= []
        f36= []
        f48= []
        f60= []
        f72= []
        f84= []
        f96= []
        f108= []
        f120= []
        f132= []
        f144= []
        f156= []
        f168= []

        forecasts = [f12,f24,f36,f48,f60,f72,f84,f96,f108,f120,f132,f144,f156,f168]
        fc_index = 0

        for line in f:
            Elements = line.split()
           
            if Elements:
                if Elements[0]=='DELV':
                    
                    if len(Elements)==9:
                        FF_Name = (Elements[2])

                        if int(FF_Name)>12:
                            fc_index += 1


                    else:
                        FF_Name = (Elements[1][2:])
                        if int(FF_Name)>11:
                            fc_index += 1


                elif Elements:
                    forecasts[fc_index].append(Elements[1:])       


    return [np.array(ff) for ff in forecasts]



def fore_int_diag(forecasts):
    F_ofcl = ['12','24','36','48','60','72','84','96','108','120','132','144','156','168']


    
    Bound_Tperc = 99.
    Bound_Lperc = 100-Bound_Tperc

    vmins = np.arange(-127.5, 127.5,5)
    vmaxs = np.arange(-122.5,132.5,5)


    mae_ints = np.zeros(shape=(len(F_ofcl),len(vmins)))
    mean_ints = np.zeros(shape=(len(F_ofcl),len(vmins)))
    std_ints = np.zeros(shape=(len(F_ofcl),len(vmins)))
    N_ints = np.zeros(shape=(len(F_ofcl),len(vmins)))
    
    bias = []
    mae = []


    i=0
    for it,fore in enumerate(forecasts):    
        #Thse are hacky work arounds to remove the 60 h point and forecast times like 156
        if (len(fore))==0:
            continue

            
        forecasted_data = fore[:,6].astype(float)
        observed_data = fore[:,5].astype(float)
        bias.append(np.nanmean(forecasted_data-observed_data))
        mae.append(np.nanmean(np.abs(forecasted_data-observed_data)))
        lower_bound = (np.percentile(forecasted_data,Bound_Lperc,interpolation='lower'))
        upper_bound = (np.percentile(forecasted_data,Bound_Tperc,interpolation='higher'))

        
        j=0
        

        for vmin,vmax in zip(vmins,vmaxs):
            if vmax<=lower_bound:
                data =  observed_data[(forecasted_data<=lower_bound)]
                data_f =  forecasted_data[(forecasted_data<=lower_bound)]

            elif vmin>=upper_bound:
                data =  observed_data[(forecasted_data>=upper_bound)]
                data_f =  forecasted_data[(forecasted_data>=upper_bound)]

                
            else:
                data =  observed_data[(forecasted_data>=vmin)&(forecasted_data<=vmax)]
                data_f =  forecasted_data[(forecasted_data>=vmin)&(forecasted_data<=vmax)]


            #If there is data in the bin
            if data.any():
                mean_ints[i,j] = np.nanmean(data_f-data)
                std_ints[i,j] = np.nanstd(data_f-data)
                mae_ints[i,j] = np.nanmean(np.abs(data_f-data))
                N_ints[i,j] = len(data_f)

 
            #If there is no forecast in the bin
            else:
                mean_ints[i,j] = np.nan
                mae_ints[i,j] = np.nan
                std_ints[i,j] = np.nan
                N_ints[i,j] = 0         
                
            j+=1
            
        i+=1
                

            
    return mean_ints,std_ints,N_ints,bias,mae,mae_ints


def True_sample(forecasts):
    F_ofcl = ['12','24','36','48','60','72','84','96','108','120','132','144','156','168']

    vmins = np.arange(-127.5, 127.5,5)
    vmaxs = np.arange(-122.5,132.5,5)


    N_ints = np.zeros(shape=(len(F_ofcl),len(vmins)))
    

    i=0
    
    for it,fore in enumerate(forecasts):    
        #Thse are hacky work arounds to remove the 60 h point and forecast times like 156
        if (len(fore))==0:
            continue
            
        forecasted_data = fore[:,6].astype(float)
        observed_data = fore[:,5].astype(float)
        
        j=0
        

        for vmin,vmax in zip(vmins,vmaxs):
            data =  observed_data[(forecasted_data>=vmin)&(forecasted_data<=vmax)]
            data_f =  forecasted_data[(forecasted_data>=vmin)&(forecasted_data<=vmax)]
            
            if data.any():
                N_ints[i,j] = len(data_f)
            else:
                N_ints[i,j] = 0 
  
                
            j+=1
            
        i+=1
                
            
    return N_ints




def get_BC(fpath):
        
    Database = read_vmod(fpath)
      
    mean_,std_,numb_,bias,mae,mae_ = fore_int_diag(Database) 
    numb_ = True_sample(Database) 

    
    return myround(mean_,1),std_,numb_,mae_


