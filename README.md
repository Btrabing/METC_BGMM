# Multi Ensemble Tropical Cyclone Bayesian Gaussian Mixture Model

Benjamin Trabing\
CIRA/CSU/NHC\
Ben.Trabing@noaa.gov

## Overview

A Bayesian Gaussian Mixture Model (BGMM) is used to cluster track forecasts from multiple global ensemble systems. Graphics are then created showing the clustered tracks, mean tracks, and intensity plots. The purpose is to isolate potential forecast scenarios for both the track and intensity. The use of a BGMM has an advantage over K-means clustering in that it clusters the path of the tropical cyclones and not just points in time. This means that BGMM clusters the tracks based on where the tropical cyclone goes instead of where it begins or where it is at a given time. 

While global ensembles have a severe under forecast bias for intensity, the improving resolution of the ensembles means that the intensity guidance may be useful soon. The GEFS has already been shown to have some intensity forecast skill over climatology. Track dependent intensity solutions should still be evident based on ensemble clustering and additional intensity bias correction techniques have been developed but not thoroughly tested. Some code within this repository uses functions from the Intensity Bias and Uncertainty Scheme (IBUS; https://gitlab.com/Btrabing/IBUS); however, it should not be used without modification. 

The number of clusters must be defined; however, an iterative approach is used to remove clusters if they are too small. As such the number of clusters will vary and not always be consistent with what is input.

## Note

This code is note optimized and still in development. Please reach out if you have any questions.



